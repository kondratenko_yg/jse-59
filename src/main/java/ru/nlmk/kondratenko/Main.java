package ru.nlmk.kondratenko;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.nlmk.kondratenko.config.HibernateConfig;
import ru.nlmk.kondratenko.model.*;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        SportsCar sportsCar = new SportsCar("Porsche","911 gt3",LocalDate.ofYearDay(2007,4),400,50);
        Bus bus = new Bus("VolgaBus","1",LocalDate.ofYearDay(1999,3),40);
        ElectricCar electricCar = new ElectricCar("Toyota","Prius",LocalDate.ofYearDay(2020,2),500);
        Truck truck = new Truck("Mitsubishi","L200", LocalDate.ofYearDay(2013,1),900);

        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

        Session session;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(sportsCar);
            session.persist(bus);
            session.persist(electricCar);
            session.persist(truck);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            throw e;
        }

        try{
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            List<Vehicle> billingDetails = session.createQuery("select v from Vehicle v").list();
            for (Object billingDetail : billingDetails) {
                System.out.println(billingDetail.toString());
            }
        } catch (Exception e){{
            transaction.rollback();
            throw e;
        }}
    }
}
