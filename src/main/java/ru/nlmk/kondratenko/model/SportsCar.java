package ru.nlmk.kondratenko.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "sports_car")
@NoArgsConstructor
public class SportsCar extends Vehicle {
    private Integer horsepower;

    private Integer acceleration;

    public SportsCar(String brand, String model, LocalDate year, Integer horsepower, Integer acceleration) {
        super(brand, model, year);
        this.horsepower = horsepower;
        this.acceleration = acceleration;
    }

    @Override
    public String toString() {
        return "SportsCar{" +
                "horsepower=" + horsepower +
                ", acceleration=" + acceleration +
                ", fromSuper=" + super.toString() +
                '}';
    }
}
