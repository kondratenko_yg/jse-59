package ru.nlmk.kondratenko.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "truck")
@NoArgsConstructor
public class Truck extends Vehicle {
    private Integer carrying;
    public Truck(String brand, String model, LocalDate year, Integer carrying) {
        super(brand, model, year);
        this.carrying = carrying;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "carrying=" + carrying +
                ", fromSuper=" + super.toString() +
                '}';
    }
}
