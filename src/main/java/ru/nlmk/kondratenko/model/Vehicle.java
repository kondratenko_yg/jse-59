package ru.nlmk.kondratenko.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "vehicle")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor
public abstract class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String brand;

    private String model;

    @Column(name = "year", columnDefinition = "TIMESTAMP")
    private LocalDate year;

    public Vehicle(String brand, String model, LocalDate year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
}
